const express = require('express');

const app = express();

app.use('/static', express.static('public'));

app.get('/api/customers', (req, res) => {
  const customers = [
    {id: 1, firstName: 'John', lastName: 'Doe'},
    {id: 2, firstName: 'Brad', lastName: 'Traversy'},
    {id: 3, firstName: 'Mary', lastName: 'Swanson'},
  ];

  res.json(customers);
});

app.get('/api/trips', (req, res) => {
  const data = [
    {
      id: "trip1",
      from: "Москва",
      to: "Десногорск",
      date: new Date(Date.UTC(2018, 11, 12, 19, 30, 0)),
      meetingPoint: "г.Москва, Парковка возле ТЦ Мебель парк",
      subway: "Румянцево",
      destination: "г.Десногорск",
      destinationSubway: null,
      coordinates: [[55.633137, 37.442715], [54.151793, 33.287138]],
      seats: 2,
      coast: 500
    },
    {
      id: "trip2",
      from: "Москва",
      to: "Тула",
      date: new Date(Date.UTC(2018, 11, 13, 14, 30, 0)),
      meetingPoint: "г.Москва, Парковка возле ТЦ Мозайка",
      subway: "Аннино",
      destination: "г.Тула",
      destinationSubway: null,
      coordinates: [[55.570770, 37.577451], [54.203550, 37.617444]],
      seats: 2,
      coast: 300
    },
    {
      id: "trip3",
      from: "Тула",
      to: "Москва",
      date: new Date(Date.UTC(2018, 11, 14, 16, 30, 0)),
      meetingPoint: "г.Тула, Парковка возле оружейного музея",
      subway: null,
      destination: "г.Москва",
      destinationSubway: "Буннинская аллея",
      coordinates: [[54.203550, 37.617444], [55.538056, 37.513870]],
      seats: 3,
      coast: 300
    }
  ]

  //console.error(err.stack);
  //res.status(500).send({ error: 'Something failed!' });

  res.json(data);
});


const port = 5000;

app.listen(port, () => `Server running on port ${port}`);
import React from 'react';

import Header from "../components/header"
import Content from "../components/content"
import Footer from "../components/footer"

import './index.css';

const App = () => {
  return (
    <div className="grid">
      <Header />
      <Content />
      <Footer />
    </div>
  );
}

export default App

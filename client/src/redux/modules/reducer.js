import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import tripsPage from './trips';


export default combineReducers({
  routing: routerReducer,
  tripsPage
});
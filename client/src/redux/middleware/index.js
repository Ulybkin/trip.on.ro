import { all, fork } from "redux-saga/effects"

import trips from "./trips"


export default function rootMiddleware() {
    return function*() {
        yield all([
            fork(trips)
        ]);
    };
}
  

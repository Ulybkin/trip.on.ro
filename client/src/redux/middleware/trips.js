import { all, call, put, takeEvery } from 'redux-saga/effects'

import {
    FETCH_TRIPS, 
    fetchTripsSucceed, 
    fetchTripsFailed
} from '../modules/trips'
import {serverError} from '../../constants/messages'
import { apiGetTrips } from '../../api'

function* trips() {
    yield all([
        takeEvery(FETCH_TRIPS, function*() {
            try {
                const data = yield call(apiGetTrips)
                yield put(fetchTripsSucceed(data));
            } catch(error) {
                yield put(fetchTripsFailed(serverError));
            } 
        })
    ])
}
  
export default trips;

//import * as _ from 'lodash'
import * as moment from 'moment'
import {Map} from 'immutable'

//преобразуем значения массива в иммутабельный объект с 
//ключами-id из элементов массива
export const arrToMap = (arr, DataRecord = Map) => 
    arr.reduce((acc, item) =>
        acc.set(item.id, new DataRecord(item))
    , new Map({}))

//Преобразуем иммутабельны объект в массив 
export const mapToArr = (obj) => 
    obj.valueSeq().toArray()

/* export const compareDates = (selectedDate, trips) => 
    trips.find(({date}) => {
        console.log('date', date, 'selectedDate', selectedDate)
        console.log( (new Date(date)) === selectedDate )
    }
) */

export const fullDate = (tripDate) =>
    moment.utc(tripDate).format('DD.MM.YYYY')
    
export const fullTime= (tripDate) => 
    moment.utc(tripDate).format('HH-mm')


    


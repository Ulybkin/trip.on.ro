import React from 'react';
import {shallow} from 'enzyme'

import {
    fetchTrips,
    fetchTripsSucceed,
    fetchTripsFailed,
    setSelectedTripId,
    resetSelectedTripId,
    reservationSeats,
    FETCH_TRIPS, 
    FETCH_TRIPS_SUCCEEDED,
    FETCH_TRIPS_FAILED,
    SET_SELECTED_TRIP_ID,
    RESET_SELECTED_TRIP_ID,
    RESERVATED_SEATS
} from '../redux/modules/trips'


describe('>>>A C T I O N --- Test tripActions', ()=>{

    it('+++ actionCreator fetchTrips', () => {
        const action = fetchTrips()
        expect(action).toEqual({ type: FETCH_TRIPS})
    });
    it('+++ actionCreator fetchTripsSucceed', () => {
        const action = fetchTripsSucceed({})
        expect(action).toEqual({ type:FETCH_TRIPS_SUCCEEDED, payload: {} })
    });
    it('+++ actionCreator fetchTripsFailed', () => {
        const action = fetchTripsFailed("Всё плохо")
        expect(action).toEqual({ type: FETCH_TRIPS_FAILED, payload: "Всё плохо"})
    });
    it('+++ actionCreator setSelectedTripId', () => {
        const action = setSelectedTripId('trip1')
        expect(action).toEqual({ type: SET_SELECTED_TRIP_ID, payload: 'trip1'})
    });
    it('+++ actionCreator resetSelectedTripId', () => {
        const action = resetSelectedTripId()
        expect(action).toEqual({ type: RESET_SELECTED_TRIP_ID})
    });
    it('+++ actionCreator reservationSeats', () => {
        const action = reservationSeats('trip1', 2)
        expect(action).toEqual({ type: RESERVATED_SEATS, payload: {id: 'trip1', value: 2}})
    });
});
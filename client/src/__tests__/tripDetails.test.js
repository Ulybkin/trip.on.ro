import React from 'react';
import {shallow} from 'enzyme'
import { TripDetails } from '../components/content/tripDetails';
import Popup from '../components/content/tripDetails/popup';

const trip = {
    id: "trip2",
    from: "Москва",
    to: "Тула",
    date: new Date(Date.UTC(2018, 11, 13, 14, 30, 0)),
    meetingPoint: "г.Москва, Парковка возле ТЦ Мозайка",
    subway: "Аннино",
    destination: "г.Тула",
    destinationSubway: null,
    coordinates: [[55.570770, 37.577451], [54.203550, 37.617444]],
    seats: 2,
    coast: 300
  }

describe('>>>TRIP DETAILS --- Shallow Render REACT COMPONENTS', () => {
    let wrapper
    const reservationSeats = jest.fn();

    beforeEach(()=>{
        wrapper = shallow(<TripDetails 
            isLoaded={false} 
            trip={trip}
            id= 'trip2'
            reservationSeats={reservationSeats}
            />)
    })

    it('+++ render the DUMB component', () => {
        expect(wrapper.length).toEqual(1)
    });

    it('initial state before events', () => {
        const isOpen = wrapper.state().isOpen
        const selectedSeats = wrapper.state().selectedSeats
        expect(isOpen).toBe(false)
        expect(selectedSeats).toBe(0)
    })

    it('selectedSeats should change after the method call', () => {
        let selectedSeats

        selectedSeats = wrapper.state().selectedSeats
        expect(selectedSeats).toBe(0)

        wrapper.instance().handleChangeSeatsValue(2)
        selectedSeats = wrapper.state().selectedSeats
        expect(selectedSeats).toBe(2)
    })

    it('<Popup /> should be visible if isOpen is true', () => {
        wrapper.setState({isOpen: true})
        expect(wrapper.find(Popup).length).toEqual(1)
    })

    it('button should be disabled if selectedSeats = 0', () => {
        const selectedSeats = wrapper.state().selectedSeats
        expect(selectedSeats).toBe(0)
        //проблема с атрибутом. решено.
        const button = wrapper.find('button')
        expect(button.is('[disabled]')).toBe(true);
    })

    it('button should send data to store, set initial data for state', () => {
        wrapper.setState({selectedSeats: 2})
        wrapper.find('button').simulate('click');

        expect(reservationSeats).toBeCalledWith('trip2', 2);

        const isOpen = wrapper.state().isOpen
        const selectedSeats = wrapper.state().selectedSeats
        expect(isOpen).toBe(true)
        expect(selectedSeats).toBe(0)
    })
})

describe('>>>POPUP --- Shallow Render REACT COMPONENTS', () => {
    let wrapper
    const togglePopup = jest.fn();

    beforeEach(()=>{
        wrapper = shallow(<Popup togglePopup={togglePopup} />)
    })

    it('button should the call method and call function from props', () => {
        //проблема с event.preventDefault(). решено.
        wrapper.find('button').simulate('click', {
            preventDefault: () => {}
        });
        expect(togglePopup.mock.calls.length).toBe(1);
    })
})

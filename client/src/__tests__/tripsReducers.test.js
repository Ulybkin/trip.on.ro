import tripsReducers from '../redux/modules/trips'
import {
    FETCH_TRIPS, 
    FETCH_TRIPS_SUCCEEDED,
    FETCH_TRIPS_FAILED,
    SET_SELECTED_TRIP_ID,
    RESET_SELECTED_TRIP_ID,
    RESERVATED_SEATS
} from '../redux/modules/trips'
import { mapToArr } from '../helpers/main'
import {Record, Map, List} from 'immutable'

const trip = {
    id: "trip2",
    from: "Москва",
    to: "Тула",
    date: "2018-12-13T14:30:00.000Z",
    meetingPoint: "г.Москва, Парковка возле ТЦ Мозайка",
    subway: "Аннино",
    destination: "г.Тула",
    destinationSubway: null,
    coordinates: [[55.570770, 37.577451], [54.203550, 37.617444]],
    seats: 2,
    coast: 300
  }

const ReducerState = new Record({
    trips: new Map({}),
    selectedTripId: null,
    isLoading: false,
    error: null
});
  
const defaultState = new ReducerState()


describe('>>>R E D U C E R --- Test tripsReducers',()=>{
    it('+++ reducer for FETCH_TRIPS', () => {
        let state = new ReducerState()
        state = tripsReducers(state, {type: FETCH_TRIPS}).toJSON()
        expect(state).toEqual({isLoading: true, error: null, "selectedTripId": null, "trips": {}})
    })

    it('+++ reducer for FETCH_TRIPS_SUCCEEDED', () => {
        let state = new ReducerState()
        state = tripsReducers(state, {type: FETCH_TRIPS_SUCCEEDED, payload: [trip]}).toJSON()
        
        expect(state).toEqual({isLoading: false, error: null, "selectedTripId": null, trips: {trip2: trip}})
        expect()
    })
    
})
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { Router } from 'react-router'
//import { syncHistoryWithStore } from 'react-router-redux';
import history from './history'

import './index.css';
import App from './app';
import createStore from './redux/create'

const store = createStore();

//dev
window.store = store


ReactDOM.render((
    <Router history={history}>
       <Provider store={store}> 
            <App />
        </Provider> 
    </Router>
), document.getElementById('root'));

export const navigation = [
    {
        id: "nav1",
        text: "Поездки",
        path: "/trips"
    },
    {
        id: "nav2",
        text: "Обо мне",
        path: "/aboutme"
    },
    {
        id: "nav3",
        text: "Кто такая РО",
        path: "/aboutro"
    },
    {
        id: "nav4",
        text: "Способы оплаты",
        path: "/payment"
    }
]

export const mainColor = '#26281d'
export const background = '#d1c9b6'
export const text = '#fb7e00'
    
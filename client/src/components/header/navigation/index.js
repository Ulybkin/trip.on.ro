import React from 'react'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router-dom'

import { Container } from "./styled"
import { navigation } from '../../../constants/general'

const Navigation = (props) => {
    const {pathname} = props.location
    return (
        <Container>
            {navigation.map(nav => 
                <Link 
                    className={pathname === nav.path ? 'active' : ''} 
                    key={nav.id} 
                    to={nav.path}
                >
                {nav.text}
                </Link>
            )}
        </Container>
    );
}

export default withRouter(Navigation)

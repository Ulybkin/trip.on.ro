import styled from "styled-components";

import {text, background} from '../../../constants/general'

export const Container =  styled.div`
    font-family: sans-serif;
    font-size: 16px;
    display: flex;
    flex-wrap: row nowrap;
    transition: font-size 1s;
    & a {
        margin-right: 10px;
        text-decoration: none;
        color: ${text};
    }

    & a:hover {
        color: ${background}; 
    }

    .active {
        color: ${background};     
    }

    @media (max-width: 710px) {
        font-size 12px; 
    }

    @media (max-width: 440px) {
        flex-direction: column;
        justify-content: center;
        a {
            text-align: center;
            margin-right: 0px;
            margin-bottom: 10px;
        }
    }
`;

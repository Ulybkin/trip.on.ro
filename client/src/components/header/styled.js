import styled from "styled-components";

export const Container =  styled.div`
    & .header-wrapper {
        display:flex;
        flex-wrap: row nowrap;
        justify-content: space-between; 
        align-items: center;
        max-width: 840px;
        margin: 0 auto;
    }

    @media (max-width: 440px) {
        .header-wrapper {
            flex-direction: column;
        }
    }
`;

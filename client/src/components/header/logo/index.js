import React from 'react'
import { Link } from 'react-router-dom'

import Image from '../../_common/image'
import { Container } from "./styled"

const Logo = () => {
    return (
        <Container>
            <Link to='/trips'>
                <Image name="logo-main" height="45px" width="70px" />
            </Link>
        </Container>
    );
}

export default Logo

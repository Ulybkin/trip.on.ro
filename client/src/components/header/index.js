import React from 'react';

import { Container } from "./styled"
import Navigation from "./navigation"
import Logo from './logo'

const Header = () => {
    return (
        <Container>
            <div className="header-wrapper">
                <Logo />
                <Navigation />
            </div>
        </Container>
    )
}

export default Header;

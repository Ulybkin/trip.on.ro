import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import Loader from '../../_common/loader'
import YaMap from '../../_common/map'
import Detail from './detail'
import Popup from './popup'
import IncrementAndDecrement from '../../_common/IncrementAndDecrement'
import { getTripById, getIsLoaded } from '../../../selectors/trips'
import { reservationSeats } from '../../../redux/modules/trips'

import {Container} from './styled'

export class TripDetails extends Component {
  static propTypes = {
    //connect
    isLoaded: PropTypes.bool,
    trip: PropTypes.object,
    id: PropTypes.string
  }

  state = {
    isOpen: false,
    selectedSeats: 0
  }

  handleReservation = () => {
    const {trip, reservationSeats} = this.props
    const {selectedSeats} = this.state
    
    reservationSeats(trip.id, selectedSeats)
    this.handleTogglePopup()
  }

  handleTogglePopup = () => {
    this.setState({
      isOpen: !this.state.isOpen,
      selectedSeats: 0
    })
  }

  handleChangeSeatsValue = (value) => {
    this.setState({selectedSeats: value})
  }

  render() {
    const {trip, isLoaded, id} = this.props
    const {selectedSeats, isOpen} = this.state
    if(isLoaded) return (<Loader />)

    return (
      <Container>
        <div className="info">
          <Detail trip={trip}/>
          <IncrementAndDecrement 
            seats={trip.seats} 
            selectedSeats= {selectedSeats}
            changeSeatsValue={this.handleChangeSeatsValue}
          />
          <button className='booking' onClick={this.handleReservation} disabled={selectedSeats === 0}>Поехали!</button>
          {isOpen && <Popup togglePopup={this.handleTogglePopup}/>}
        </div>
        <YaMap id={id}/>
      </Container>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
    const id = ownProps.match.params.id
    return {
        isLoaded: getIsLoaded(state),
        trip: getTripById(state, id),
        id
    }
}

export default connect(mapStateToProps, {reservationSeats})(TripDetails)

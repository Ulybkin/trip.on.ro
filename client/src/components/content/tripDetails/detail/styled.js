import styled from "styled-components";

import {background} from '../../../../constants/general'

export const Container =  styled.div`
    font-size: 20px;
    display: flex;
    flex-flow: column nowrap;
    align-items: center;
    text-align: center;
    & > div {
        margin-bottom: 10px;
    }

    & img {
        margin-bottom: 2px;
    }

    & .title {
        display: flex;
        flex-flow: column  nowrap;
        align-items: center;
        & span:first-child {
            border-bottom: 1px solid white;
        }
    }

    .seatsCount {
        color: ${background};
    }

    .valueCoast {
        color: ${background};
    }
`;

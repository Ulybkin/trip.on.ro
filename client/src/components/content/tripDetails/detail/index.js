import React from 'react'
import PropTypes from 'prop-types'

import Image from '../../../_common/image'
import Subway from '../../../_common/subway'
import { Container } from './styled'
import { fullDate, fullTime } from '../../../../helpers/main'

const propTypes = {
    trip:PropTypes.shape({
      from: PropTypes.string, 
      to: PropTypes.string, 
      date: PropTypes.date,
      subway: PropTypes.oneOfType([null, PropTypes.string]), 
      //subway: PropTypes.any,
      destinationSubway: PropTypes.oneOfType([null, PropTypes.string]),
      //destinationSubway: PropTypes.any,
      meetingPoint : PropTypes.string,
      destination: PropTypes.string,
      seats: PropTypes.number,
      coast: PropTypes.number
    })
    
}

const Detail = ({trip:{from, to, date,  subway, destinationSubway, meetingPoint, destination, seats, coast}}) => {
  const tripDate = fullDate(date)
  const tripTime = fullTime(date)
  //TODO: переписать в читаемый вид
  return (
    <Container>
        <div className="route">
          {`${from} `} → {` ${to}`} 
        </div>
        <div className="date title">
          <Image name='calendar' width='20px' height='20px'/>
          <span>{tripDate} в {tripTime}</span>
        </div>
        <div className="meetingPoint title">
          <Image name='from' width='16px' height='20px'/>
          <span>{meetingPoint}</span>
          {subway ? <Subway text={subway} /> : false}
        </div>
        <div className="destination title">
          <Image name='to' width='16px' height='20px'/>
          <span>{destination}</span>
          {destinationSubway ? <Subway text={destinationSubway} /> : false  }
        </div>
        <div className='cost'>
          <span>Стоимость проезда - </span>
          <span className='valueCoast'>{coast}</span>
          <span> рублей</span>
        </div>
        <div className="seats">
          <span>Количество свободных мест - </span>
          <span className='seatsCount'>{seats}</span>
        </div>
    </Container>
  )
}

Detail.propTypes = propTypes

export default Detail

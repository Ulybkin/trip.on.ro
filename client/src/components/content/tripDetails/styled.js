import styled from "styled-components";

import {text, mainColor, background} from '../../../constants/general'

export const Container =  styled.div`
    width: 100%
    color: ${text};
    & .info {
        display: flex;
        flex-flow: column nowrap;
        align-items: center;
        margin-bottom: 10px;
    }

    & .popup {
        z-index: 10
    }

    & .booking {
        cursor:pointer;
        margin-top: 10px;
        padding: 8px;
        color: ${mainColor};
        border: none;
        outline: none;
        background-color: ${background};
        border: 1px solid ${background};
        border-radius: 4px;
        box-shadow: 0px 2px 0px #6D7253, 0px 7px 25px rgba(0,0,0,.7);
        transition: all .1s ease;
    }

    & .booking: active {
        box-shadow: 0px 1px 0px rgba(0,0,0,1), 0px 3px 6px rgba(0,0,0,.9);
        position: relative;
        top: 2px;
    }

    & .booking: disabled {
        filter: grayscale(100%);
    }

    `;

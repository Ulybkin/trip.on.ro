import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Container } from './styled'

class Popup extends Component {
  static propTypes = {
    togglePopup: PropTypes.func
  }
  
  handleClose = (event) => {
    this.props.togglePopup()
    event.preventDefault()
  }

  render() {
    return (
      <Container className="popup">
        <div className="content">
            <p>Спасибо за бронирование)))</p>
            <button onClick={this.handleClose}>Закрыть</button>
        </div>
      </Container>
      
    )
  }
}

export default Popup

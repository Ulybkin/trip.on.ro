import styled from "styled-components";


export const Container =  styled.div`
    width:100%;
    height: 100%;
    background-color: rgba(0,0,0,0.5);
    overflow:hidden;
    position:fixed;
    top:0px;
    left: 0px;
    display: flex;
    align-items: center;
    align-content: center; 
    justify-content: center; 
    
    & .content {
        width:200px;
        height: 200px;
        padding:10px;
        background-color: #c5c5c5;
        border-radius:5px;
        font-size: 1rem;
        display: flex;
        flex-flow: column nowrap;
        align-items: center;

        & button {
            cursor:pointer;
        }
    }

`;
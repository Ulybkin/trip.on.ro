import React from 'react';

import { Container } from './styled'
import Image from '../../_common/image'

const AboutRO = () => {
    return (
        <Container>
            <span className='ro-title'>РО ^^</span>
            <div className='ro-info'>
                <Image name="rio2" width='300px' height='200px'/>
                <div className='ro-text'>
                    <span>РО - это моя маленькая Rock'n'Rolla</span>
                    <span>Поедем куда угодно, но только не в грязь.</span>
                    <span>Выбирайте поездку и потелели! </span>
                </div>
            </div>
        </Container>
    );
};

export default AboutRO;

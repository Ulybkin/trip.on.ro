import styled from "styled-components";

import { text, background } from '../../../constants/general'

export const Container =  styled.div`
    & img {
        border-radius: 100px;
        border: 1px solid ${background};
    }

    display: flex;  
    flex-flow: column nowrap;
    align-items: center;
    color: ${text};
    font-size: 20px;

    & .ro-title {
        font-size: 26px;
        margin-bottom: 10px;
    }

    & .ro-info {
        display: flex;  
        flex-flow: column nowrap;
        justify-content: space-between; 
        align-items: center;

        & .ro-text {
            margin: 10px 0 5px 0;
            display: flex;  
            flex-flow: column nowrap;
            justify-content: flex-start; 
            align-items: center;
            
        }
    }

    @media (max-width: 680px) {
        font-size: 18px;

        & .ro-title {
            font-size: 20px; 
        }
    }

    @media (max-width: 490px) {
        & img {
            width: 200px;
            height: 150px;
        }

        & .ro-text {
            & span {
                text-align: center;
            }
        }
    }
`;

import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { Container } from './styled'
import { getIsLoaded, getSelectedTripId } from '../../../selectors/trips'
import Loader from '../../_common/loader'
import TripsList from './tripsList'
import Hint from './hint'
import Calendar from '../../_common/calendar'
import Popup from './popup'
import YaMap from '../../_common/map'


const propTypes = {
    //connect
    isLoaded: PropTypes.bool,
    selectedTripId: PropTypes.oneOfType([null, PropTypes.string]),
    //selectedTripId: PropTypes.any,
}

const Trips = ({isLoaded, selectedTripId}) => {
    
    if(isLoaded) return (<Loader />)
    return (
        <Container>
            <div className="trips-top">
                <TripsList />
                <Calendar />
                {selectedTripId && <Popup />}
            </div>
            <Hint />
            <YaMap/>
        </Container>
    )
    
    
}

Trips.propTypes = propTypes

const mapStateToProps = (state) => {
    return ({
        isLoaded: getIsLoaded(state),
        selectedTripId: getSelectedTripId(state)
    })
}

export default connect(mapStateToProps)(Trips)


import styled from "styled-components";

import { text } from '../../../../constants/general'

export const Container =  styled.div`
    color: ${text};
    margin-bottom: 10px;
    text-align: center;
`;

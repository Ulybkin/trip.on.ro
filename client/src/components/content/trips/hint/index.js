import React from 'react'

import {Container} from './styled'

const Hint = () => {
  return (
    <Container>
      * Выбрать поездку можно путём нажатия на список или подсвеченные даты
    </Container>
  )
}

export default Hint

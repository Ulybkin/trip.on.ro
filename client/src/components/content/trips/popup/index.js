import React, {Component} from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import Image from '../../../_common/image'
import history from '../../../../history'
import { getSelectedTripById } from '../../../../selectors/trips'
import { resetSelectedTripId } from '../../../../redux/modules/trips'
import { fullDate } from '../../../../helpers/main';

import { Container } from './styled'

class Popup extends Component {
    static propTypes = {
        trip: PropTypes.object,
    }

    handleClose = (event) => {
        this.props.resetSelectedTripId()
        event.preventDefault()
    }

    handleMoreDetail = (event) => {
        this.props.resetSelectedTripId()
        history.push(`/trips/${this.props.trip.id}`)
        event.preventDefault()
    }

    render() {
        const {from, to, date} = this.props.trip
        const tripDate = fullDate(date)
        return (
            <Container className="popup">
                <div className="content">
                    <div className="closeIcon" onClick={this.handleClose} >
                        <Image name="close" height="20px" width="20px" />
                    </div>
                    <span>Поездка:</span>
                    <span>{from}-{to}</span>
                    <span>{tripDate}</span>
                    <button onClick={this.handleMoreDetail}>Больше информации</button>
                </div>
            </Container>
        ) 
    }
}

const mapStateToProps = (state) => ({
    trip: getSelectedTripById(state)
})

export default connect(mapStateToProps, {resetSelectedTripId})(Popup)

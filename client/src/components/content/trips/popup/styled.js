import styled from "styled-components";

import {text, background, mainColor} from '../../../../constants/general'

export const Container =  styled.div`
    background-color: rgba(0,0,0,0.5);
    overflow:hidden;
    position:fixed;
    top:0;
    bottom:0;
    left:0;
    right:0;
    display: flex;
    align-items: center;
    align-content: center; 
    justify-content: center; 
    
    & .content {
        width:210px;
        height: 165px;
        padding:10px;
        background-color: ${background};
        border-radius:5px;
        font-size: 18px;
        display: flex;
        flex-flow: column nowrap;
        align-items: center;

        & .closeIcon {
            cursor:pointer;
            align-self: flex-end;
        }

        & span {
            color: ${mainColor};
            margin-bottom: 8px;
        }

        & button {
            cursor:pointer;
            margin-top: 10px;
            padding: 8px;
            color: ${text};
            border: none;
            outline: none;
            background-color: ${mainColor};
            border: 1px solid ${mainColor};
            border-radius: 4px;
            box-shadow: 0px 4px 0px #6D7253, 0px 7px 25px rgba(0,0,0,.7);
            transition: all .1s ease;
        }

        & button: active {
            box-shadow: 0px 1px 0px rgba(0,0,0,1), 0px 3px 6px rgba(0,0,0,.9);
            position: relative;
            top: 3px;
        }
    }

`;

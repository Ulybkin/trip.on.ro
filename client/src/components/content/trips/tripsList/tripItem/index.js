import React, {Component} from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { setSelectedTripId } from '../../../../../redux/modules/trips'
import { fullDate } from '../../../../../helpers/main'
import Image from '../../../../_common/image'
import { Container } from './styled'


class TripItem extends Component {
    static propTypes = {
        trip: PropTypes.object,
    }
    
    handleClick = (event) => {
        const {setSelectedTripId, trip:{id}} = this.props
        setSelectedTripId(id)
        event.preventDefault()
    }

    render() {
        const {from, to, date} = this.props.trip
        const tripDate = fullDate(date)
        return (
            <Container onClick={this.handleClick}>
                <Image name="bambcar" height="20px" width="25px"/>
                <span>{from}-{to} {tripDate}</span>
            </Container>
        )
    }
}

export default connect(null, {setSelectedTripId})(TripItem)
import styled from "styled-components";
import {text} from '../../../../../constants/general'

export const Container =  styled.div`
    font-family: sans-serif;
    font-size: 14px;
    display: flex;
    flex-flow: row nowrap;  
    align-items: center;
    cursor: pointer; 
    margin-top: 5px;
    color: ${text};
    & span {
        margin: 5px 0 10px 10px;
    }

    @media (max-width: 600px) {
        flex-flow: column nowrap;
    } 
`;

import styled from "styled-components";

import {background} from '../../../../constants/general'

export const Container =  styled.div`
    display: flex;
    flex-flow: column nowrap;
    align-items: flex-start;
    margin-top: 10px;
    color: ${background};

    @media (max-width: 600px) {
        align-items: center;
    } 
    
`;

import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { Container } from './styled'
import { getTripsArray } from '../../../../selectors/trips'
import TripItem from './tripItem'

const propTypes = {
    //connect
    trips: PropTypes.array
}

const TripsList = ({trips}) => {
    return (
        <Container className='tripList'>
            <span>Планируемые поездки:</span>
            {trips.map(trip => <TripItem key={trip.id} trip = {trip}/>)}
        </Container>
    );
}

TripsList.propTypes = propTypes

const mapStateToProps = (state) => {
    return {
        trips: getTripsArray(state)
    }
}

export default connect(mapStateToProps)(TripsList)
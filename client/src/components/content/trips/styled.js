import styled from "styled-components";


export const Container =  styled.div`
    
    .trips-top {
        display:flex;
        flex-flow: row nowrap;
        justify-content: space-between; 
        max-width: 840px;
        margin: 0 auto;
        & .popup {
            z-index: 10
        }
    }

    @media (max-width: 600px) {
        .trips-top {
            flex-flow: column nowrap;
            align-items: center;
        }

        .tripList {
            margin-bottom: 8px;
        }
    } 
`;

import React from 'react';

import { Container } from './styled'

const Payment = () => {
    return (
        <Container>
            <span className='payment-title'>Оплата</span>
            <span className='payment-text'>Наличные.</span>
            <span className='payment-text'>Буду признателен за оплату во время заправки)))</span>
        </Container>
    );
};

export default Payment;

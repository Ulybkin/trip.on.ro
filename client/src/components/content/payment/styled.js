import styled from "styled-components";

import { text } from '../../../constants/general'


export const Container =  styled.div`
    display: flex;
    flex-flow: column nowrap;
    align-items: center;
    color: ${text};
    font-size: 20px;

    & .payment-title {
        font-size: 26px;
        margin-bottom: 10px;
    }

    @media (max-width: 680px) {
        font-size: 18px;

        & .payment-title {
            font-size: 20px; 
        }
    }

    @media (max-width: 490px) {
        font-size: 16px;
        & img {
            width: 200px;
            height: 150px;
        }

        & .payment-text {
            text-align: center;
        }
    }
`;

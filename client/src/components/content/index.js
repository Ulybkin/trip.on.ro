import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, Switch, Redirect } from "react-router"
import { withRouter } from 'react-router-dom'

import { Container } from "./styled"
import { fetchTrips } from '../../redux/modules/trips'
import Trips from "./trips"
import TripDetails from './tripDetails'
import AboutMe from './aboutMe'
import AboutRO from './aboutRO'
import Payment from './payment'
import NotFound from "./notFound"


class Content extends Component {

    componentDidMount() {
        this.props.fetchTrips()
    } 
    
    render() {
        return (
            <Container>
                <Switch>
                    <Redirect from="/" exact to="/trips" />
                    <Route path="/trips" exact component={Trips} />
                    <Route path="/trips/:id" component={TripDetails} />
                    <Route path="/aboutme" component={AboutMe} />
                    <Route path="/aboutro" component={AboutRO} />
                    <Route path="/payment" component={Payment} />
                    <Route component={NotFound} />
                </Switch>
            </Container>
        )
    }
    
}

export default withRouter(connect(null, { fetchTrips })(Content))
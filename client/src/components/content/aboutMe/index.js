import React from 'react';

import { Container } from './styled'
import Image from '../../_common/image'

const AboutMe = () => {
    return (
        <Container>
            <span className='me-title'>Всем привет! Меня зовут Марк :)</span>
            <div className='me-info'>
                <Image name="me" width='200px' height='200px'/>
                <div className='me-text'>
                    <span>Это моя страница, куда я выкладываю все свои предстоящие поездки.</span>
                    <span>Да-да-да. Это аналог BlaBlaCar, но только для одного человека. Кстати, вот <a href="https://www.blablacar.ru/">мой профиль</a>.</span>
                    <span>Вожу уже довольно давно, так что можете не волноваться :)</span>
                    <span>На этом сайте так же можно выбрать поездку и забронировать место. </span>
                    <span>Жду Вас в свою компанию!</span>
                </div>
            </div>
        </Container>
    );
};

export default AboutMe;

import styled from "styled-components";

import { text, background } from '../../../constants/general'

export const Container =  styled.div`
    & img {
        border-radius: 200px;
        border: 1px solid ${background};
    }

    a {
        text-decoration: none;
    }
    a:visited {
        color: ${background}; 
    }

    a:active {
        color: ${background}; 
    }

    display: flex;  
    flex-flow: column nowrap;
    align-items: center;
    color: ${text};
    font-size: 20px;

    & .me-title {
        font-size: 26px;
        margin-bottom: 10px;
    }

    & .me-info {
        display: flex;  
        flex-flow: column nowrap;
        justify-content: space-between; 
        align-items: center;

        & .me-text {
            margin: 10px 0 5px 0;
            display: flex;  
            flex-flow: column nowrap;
            justify-content: flex-start; 
            align-items: center;

            & span {
                text-align: center;
            }
            
        }
    }

    @media (max-width: 680px) {
        font-size: 16px;

        & .me-title {
            font-size: 20px; 
        }
    }

    @media (max-width: 560px) {
        font-size: 14px;

        & .me-title {
            font-size: 20px; 
        }
    }

    @media (max-width: 490px) {
        font-size: 12px;

        & .me-title {
            font-size: 16px; 
        }

        & img {
            width: 100px;
            height: 100px;
        }

        & .me-text {
            & span {
                text-align: center;
            }
        }
    }

`;

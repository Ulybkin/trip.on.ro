import styled from "styled-components";

import {text, background } from '../../../constants/general'

export const Container =  styled.div`
    a:visited {
        color: ${text}; 
    }

    a:active {
        color: ${text}; 
    }
    & a {
        display: flex;
        flex-flow: row nowrap;
        align-items: center;
        text-decoration: none;
    }

    & a:hover {
        color: ${background}; 
    }

    
    & img {
        margin-right: 7px;
    }

   
`;

import React from 'react';
import PropTypes from 'prop-types';

import Image from '../../_common/image'
import { Container } from './styled'

const propTypes = {
    name: PropTypes.string,
    text: PropTypes.string,
    href: PropTypes.string
};


const Social = ({name, text, href}) => {
    return (
        <Container>
            <a href={href}>
                <Image name={name} width='15px' height='15px'/>
                <span>{text}</span>
            </a> 
        </Container>
    );
};


Social.propTypes = propTypes;


export default Social;

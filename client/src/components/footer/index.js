import React from 'react'

import Social from './social'
import Image from '../_common/image'
import { Container } from './styled'

const Footer = () => {
    return (
        <Container>
            <div className="footer-wrapper">
                <div className="phone">
                    <Image name="phone" width='15px' height='15px'/>
                    <span>+7(910)329-12-22</span>
                </div>
                <Social name='vk' text='вКонтакте' href='http://vk.com/koekto93'/>
                <Social name='instagram' text='Instagram' href='http://instagram.com/koekto93'/>
            </div>
        </Container>
    );
}

export default Footer

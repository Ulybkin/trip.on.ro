import styled from "styled-components";
import {text} from '../../constants/general'

export const Container =  styled.div`
    .footer-wrapper {
        display: flex;
        flex-flow: row nowrap;
        justify-content: space-between; 
        max-width: 840px;
        margin: 0 auto;
    }
    & .phone {
        display: flex;
        flex-flow: row nowrap;
        align-items: center;
        color: ${text};
        & img {
            margin-right: 7px;
        }
    }

    @media (max-width: 480px) {
        .footer-wrapper {
            font-size: 12px;
            & > div {
                margin-bottom: 10px;
            }
        }
        
    }
`;

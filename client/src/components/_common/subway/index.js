import React from 'react';
import PropTypes from 'prop-types';

import Image from '../image'
import { Container } from './styled'

const propTypes = {
    text: PropTypes.string
};


const Subway = ({text}) => {
    return (
        <Container>
            <Image name='subway' width='15px' height='15px' />
            &nbsp;
            -
            &nbsp;
            <span>{text}</span>
        </Container> 
    );
};


Subway.propTypes = propTypes;


export default Subway;

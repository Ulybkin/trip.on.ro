import styled from "styled-components";

export const Container =  styled.div`
    position: relative;
    height: 300px;
    z-index: 1;
    max-width: 840px;
    margin: 0 auto;
    filter: grayscale(100%);/* Изменил на серый вид */
    & .layer {
        display: block;
        position: absolute;
        width: 100%; 
        height: 300px;
        
    }
`;

import React, {Component} from 'react'
import { YMaps, Map } from 'react-yandex-maps';
import {connect} from 'react-redux'
import * as _ from 'lodash'

import { getTripById } from '../../../selectors/trips'
import {Container} from './styled'



const mapState = { center: [55.76, 37.64], zoom: 10, controls: ['searchControl'] };

class YaMap extends Component {
    
    map = null;

    handleApiAvaliable = (ymaps, route) => {
        
            ymaps.route(
                route,
                {
                    mapStateAutoApply: true,
                    boundsAutoApply: true
                }
            )
            .then(route => {
                route.getPaths().options.set({
            
                strokeColor: "0000ffff",
                opacity: 0.5, 
                });
    
                this.map.geoObjects.add(route);
            })
        
    }

    render() {
        let route = []
        if(this.props.trip) route = this.props.trip.coordinates
        return (
            <Container>
                <div className="layer">
                    <YMaps onApiAvaliable={ymaps => this.handleApiAvaliable(ymaps, route)} >
                        <Map 
                            state={mapState} 
                            instanceRef={ref => (this.map = ref)} 
                            width="100%" 
                            height="100%" 
                        >
                        </Map>
                    </YMaps>
                </div>
            </Container>

        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const id = _.get(ownProps, 'id')
    if(id) return {trip: getTripById(state, id)}
    return {}
} 

export default connect(mapStateToProps)(YaMap)
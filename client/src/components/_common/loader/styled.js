import styled, { keyframes } from "styled-components";


const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

export const Container =  styled.div`
    position: fixed;
    bottom: 0;
    top: 0;
    left: 0;
    right: 0;

    & .bg_layer {
        background-color: black;
        position: absolute;
        bottom: 0;
        top: 0;
        left: 0;
        right: 0;
        opacity: 0.25;
        z-index: 15;
    }

    & .loader {
        position: absolute;
        left: 50%;
        top: 50%;
        width: 45px;
        height: 45px;
        animation: ${rotate360} 2s linear infinite;
        z-index: 16;
    }
`;
import React from 'react'

import { Container } from './styled'
import Image from '../image'

const Loader = () => {
  return (
    <Container>
      <div className="bg_layer"></div>
      <div className="loader">
        <Image name="wheel" height="45px" width="45px" />
      </div>
    </Container>  
  )
}

export default Loader

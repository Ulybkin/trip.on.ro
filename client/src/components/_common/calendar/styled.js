import styled from "styled-components";

import {mainColor, background} from '../../../constants/general'

export const Container =  styled.div`
    & .DayPicker-Caption {
        color: ${background};
    }
    
    & .DayPicker-Day {
        color: ${background};
    }

    & .DayPicker-Day:hover {
        background-color: ${background} !important;
        color: ${mainColor};
    }
   
`;

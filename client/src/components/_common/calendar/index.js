import React, {Component} from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import DayPicker, { DateUtils } from 'react-day-picker'
import 'react-day-picker/lib/style.css'

import {Container} from './styled'
import { getDatesOfTrip, getTripsArray } from '../../../selectors/trips'
import { setSelectedTripId } from '../../../redux/modules/trips'
import { text } from '../../../constants/general'




const modifiersStyles = {
    dates: {
      color: text,
      
    },
    outside: {
      backgroundColor: "transparent",
    },
};

class Calendar extends Component {
    static propTypes = {
      //connect
      trips: PropTypes.array,
      datesOfTrip: PropTypes.array
    }

    handleDayClick = (day) => {
      const {trips, setSelectedTripId} = this.props
      const selectedTrip = trips.find(({date}) => 
        DateUtils.isSameDay(new Date(date), day)
      )
      
      selectedTrip && setSelectedTripId(selectedTrip.id)

    }

    render() {
      const modifiers = {        
        dates: this.props.datesOfTrip,
      }
      return (
        <Container>
          <DayPicker
              month={new Date(2018, 11)}
              modifiers={modifiers}
              modifiersStyles={modifiersStyles}
              onDayClick={this.handleDayClick}
          />
        </Container>
      )
    } 
}

const mapStateToProps = (state) => ({
  trips: getTripsArray(state),
  datesOfTrip: getDatesOfTrip(state)
})

export default connect(mapStateToProps, {setSelectedTripId})(Calendar) 


import React from 'react'

//import { Container } from "./styled"
import PropTypes from 'prop-types'

const propTypes = {
    name: PropTypes.string.isRequired,
    height: PropTypes.string,
    width: PropTypes.string
}

const Image = ({name, height="45px", width="45px"}) => {
    return (
        <img src={`/static/${name}.png`} width={width} height={height} alt={name}/>
    );
}

Image.propTypes = propTypes

export default Image

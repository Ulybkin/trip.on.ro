import styled from "styled-components";

export const Container =  styled.div`
    display: flex;
    flex-flow: column nowrap;
    align-items: center;
    margin-bottom: 10px;
    & .control {
        display: flex;
        flex-flow: row nowrap;
        align-items: center;
        margin-top: 5px;

        & button {
            outline: none;
        }
    }
    
    & .decrement {
        border-radius: 3px 0 0 3px;
        
    }

    & .increment {
        border-radius: 0 3px 3px 0;
    }

    & .control-place {
        padding: 1px 50px;
        background-color:#d1c9b6;
    }

`;

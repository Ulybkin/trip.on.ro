import React, { Component } from 'react'
import PropTypes from 'prop-types'

import {Container} from './styled'

export class IncrementAndDecrement extends Component {
  static propTypes = {
    changeSeatsValue: PropTypes.func, 
    seats: PropTypes.number, 
    selectedSeats: PropTypes.number
  }

  handleIncrement = (event) => {
    const {changeSeatsValue, seats, selectedSeats} = this.props
    if(selectedSeats < seats) {
        changeSeatsValue(selectedSeats + 1)
      }
    event.preventDefault()
  }

  handleDecrement = (event) => {
    const {changeSeatsValue, selectedSeats} = this.props
    if(selectedSeats > 0) {
      changeSeatsValue(selectedSeats - 1)
    }
    event.preventDefault()
  }

 /*  handleChangeValue = (event) => {
    const value = event.target.value
    const {changeSeatsValue, seats} = this.props
    if(value < seats) {
      changeSeatsValue(value)
    }
    //вывести предупреждение, если перебрали или написали маленькое число 
  } */

  render() {
    const {selectedSeats, seats} = this.props

    if(seats === 0) return (
      <span>Извините, места закончились</span>
    )

    //          <input value={this.props.selectedSeats} onChange={this.handleChangeValue}></input>

    return (
      <Container>
        <span>Выберете количество</span>
        <div className='control'>
          <button className='decrement' onClick={this.handleDecrement} disabled={selectedSeats === 0 }>-</button>
          <span className='control-place'>{this.props.selectedSeats}</span>
          <button className='increment' onClick={this.handleIncrement} disabled={selectedSeats === seats}>+</button>
        </div>
      </Container>
    )
  }
}

export default IncrementAndDecrement



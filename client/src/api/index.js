// @flow
import axios from 'axios'

 export const apiGetTrips = () =>
  axios.get("/api/trips")
    .then(({data}) => data)
    
   /*  export const apiLoadProductFiltersForType = (productType: string) =>
  axios
    .get(`/api/api/catalog/v1/products/filters/${productType}`)
    .then(({ data }) => data); */
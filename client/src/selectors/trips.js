// @flow
import * as _ from 'lodash'
import { createSelector } from 'reselect'
import { mapToArr } from '../helpers/main'

export const getIsLoaded = state =>
  _.get(state, ['tripsPage', 'isLoading'])

export const getSelectedTripId = state => 
  _.get(state, ['tripsPage', 'selectedTripId']) 

export const getTrips = state => 
  _.get(state, ['tripsPage', 'trips']) 
  
export const getTripsArray = createSelector(
  getTrips,
  trips => mapToArr(trips) || []
)

export const getSelectedTripById = createSelector(
  getTrips,
  getSelectedTripId,
  (trips, id) => trips.get(id)
)

//костыль... Получаем данные на основе айди из url
export const getTripById = (state, id) => 
_.get(state, ['tripsPage', 'trips']).get(id) || {}


//создаём массив из дат поездок
export const getDatesOfTrip = createSelector(
  getTripsArray,
  trips => trips && trips.map(({date}) => new Date(date))
)

